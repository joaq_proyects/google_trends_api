
import time
import pytrends_api_client, settings, data_layer, proxies_handler
import threading, schedule


def insert_update_handler(dl_obj, game_id, data_list, ins_upd = {}):
    def threaded_insert_update_handler(dl_obj):
        data_list_copy = list(data_list)
        for data in data_list_copy:
            if( dl_obj.check_games_value_x_day( game_id, data[0] ) ):
                method = dl_obj.update_games_value_x_day
                ins_upd["upd"] += 1
            else:
                method = dl_obj.insert_games_value_x_day
                ins_upd["ins"] += 1
            try:
                method( game_id, data[0], data[1] )
            except Exception as ex:
                print("Error when inserting/updating game:" + str(ex))
                try:
                    dl_obj.close()
                except:
                    pass
                try:
                    dl_obj = data_layer.DataLayer(settings.db_data)
                except:
                    print("NEW ERROR WHEN CONNECTING TO DB")
        try:
            dl_obj.insert_games_updated(game_id)
        except Exception as ex:
            print("Error when updating retrieved game:" + str(ex))
            try:
                dl_obj.close()
            except:
                pass
            try:
                dl_obj = data_layer.DataLayer(settings.db_data)
            except:
                print("NEW ERROR WHEN CONNECTING TO DB")
    sql_thread = threading.Thread(target = threaded_insert_update_handler, args = (dl_obj,))
    sql_thread.start()
    return sql_thread


def loop_over_games(games_names_list, thread_num, proxies_list):
    ins_upd = {"ins":0, "upd":0}
    dl_obj = None
    try:
        dl_obj = data_layer.DataLayer(settings.db_data)
    except:
        print("ERROR WHEN CONNECTING TO DB")
        return
    proxy_in_use = 0
    api_obj = pytrends_api_client.PytrendsApiClient(proxy = proxies_list[proxy_in_use]["proxy"], default_kw = settings.default_kw)
    data_list = list()
    aux_log = ""
    for game in games_names_list:
        kw_list = [game[1].replace("'", "''").replace("\"", "\\\"")] # game name
        retry = True
        while(retry):
            try:
                retry = False
                aux_log = "OK"
                res = api_obj.req_interest_over_time(kw_list)
            except Exception as ex:
                print("\t" + str(ex))
                aux_log = "ERR"
                if("400" not in str(ex)): # error code 400 means bad requests, and it may be caused by a malformed game name
                    proxy_in_use += 1
                    if(proxy_in_use < len(proxies_list)):
                        del api_obj
                        api_obj = pytrends_api_client.PytrendsApiClient(proxy = proxies_list[proxy_in_use]["proxy"], default_kw = settings.default_kw)
                        retry = True
                        time.sleep(settings.sleep_time)
                    else:
                        print("thread:" + str(thread_num) + "->all proxies blocked!!")
                        dl_obj.close()
                        return
        if(aux_log != "ERR"):
            for index, row in res.iterrows():
                data_list.append( (index, row[0]) )
            if(aux_log == "OK" and len(data_list) == 0 ):
                aux_log = "WARN"
            print("thread:" + str(thread_num) + "->" + str(game[1].replace('\n','').replace('\r','')) + "(" + str(game[0]) + "):" + str(len(data_list)) + "::" + aux_log)
        sql_thread = insert_update_handler(dl_obj, game[0], data_list, ins_upd)
        data_list.clear()
        time.sleep(settings.sleep_time)
        # this is for waiting thread to execute inserts/updates block
        aux_count = 0
        while(sql_thread.is_alive() and aux_count < settings.sleep_time):
            time.sleep(1)
            aux_count += 1
    dl_obj.close()
    print( "thread:" + str(thread_num) + "->FINISH: inserts:" + str(ins_upd["ins"]) + ";updates:" + str(ins_upd["upd"]) )


def get_sublists(a_list, divisions):
    total = len(a_list)
    subq = int(total / divisions)
    new_list = list()
    ini = 0
    for div in range(0, divisions):
        new_list.append( list() )
        for i in range(ini, ini + subq):
            new_list[div].append(a_list[i])
        ini += subq
    for i in range(ini, total):
        new_list[len(new_list)-1].append(a_list[i])
    return new_list


def get_games_to_retrieve(clear_list = False):
    dl_obj = data_layer.DataLayer(settings.db_data)
    if(clear_list):
        dl_obj.clear_games_updates()
    games_names_list = dl_obj.select_games(settings.max_games_to_select) # Returns a list of tuples
    dl_obj.close()
    return games_names_list


def exec_prog():
    ph_obj = proxies_handler.ProxyHandler()
    n_threads = settings.number_of_threads
    proxies_by_thread = settings.proxies_by_thread
    proxies_list = get_sublists( ph_obj.get_random_n_proxies( n_threads * proxies_by_thread ), n_threads )
    games_names_list = get_games_to_retrieve(False)
    print("list to retrieve:" + str(len(games_names_list)))
    if(len(games_names_list) < 100):
        games_names_list = get_games_to_retrieve(True)
    games_names_list_sublisted = get_sublists(games_names_list, n_threads)
    thread_num = 1
    for sub_list in games_names_list_sublisted:
        threading.Thread(target = loop_over_games, args = (sub_list, thread_num, proxies_list[thread_num-1])).start()
        thread_num += 1


exec_prog()
schedule.every().day.at(settings.exec_hour).do(exec_prog)
while(True):
    schedule.run_pending()
    time.sleep(1800)
