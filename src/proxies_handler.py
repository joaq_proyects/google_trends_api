
import json, random, time
import settings
import requests

class ProxyHandler:

    proxies_list_source = None
    proxies_list = None


    def __init__(self):
        self.proxies_list_source = None
        self.parse_proxy_list_with = self.settings_proxy_list
        self.parse_num_of_request = 1
        self.type_of_proxy = None
        self.proxies_list = list()
        self.get_proxies_list()
        self.check_proxies_state()


    def settings_proxy_list(self, p_list):
        self.type_of_proxy = "https"
        return settings.proxies_list


    def check_proxies_state(self):
        if(self.proxies_list is not None):
            for p in self.proxies_list:
                p["in_use"] = False
                # IMPLEMENT SOMETHING TO CHECK HEALTH


    def get_proxies_list(self):
        aux_list = list()
        if(self.proxies_list_source is not None):        
            for i in range(0, self.parse_num_of_request):
                res = requests.get(self.proxies_list_source)
                time.sleep(2)
                print("get_proxies_list:" + str(res.status_code))
                if(res.status_code == 200):
                    aux_list.append( json.loads(res.text) )
        aux_list = self.parse_proxy_list_with(aux_list)
        aux_list = aux_list[0:-1]
        count = 0
        for prx in aux_list:
            count += 1
            proxy = {self.type_of_proxy: prx.replace('\r','')}
            self.proxies_list.append( {"id":count,"proxy":proxy,"active":True,"in_use":False} )


    def get_random_n_proxies( self, p_cant ):
        max_pos = len(self.proxies_list) - 1
        proxies_groups = list()
        aux_count = 0
        if(max_pos >= p_cant):
            while(len(proxies_groups) < p_cant and aux_count <= max_pos):
                aux_count += 1
                pos = random.randint(0, max_pos)
                if(self.proxies_list[pos]["active"] and not self.proxies_list[pos]["in_use"]):
                    proxies_groups.append(self.proxies_list[pos])
                    self.proxies_list[pos]["in_use"] = True
        else:
            print("arg 1 length must be lower than arg 2 (%s)" % (str(max_pos) + "<=" + str(p_cant),))
        print("proxies_groups:" + str(proxies_groups))
        return proxies_groups
