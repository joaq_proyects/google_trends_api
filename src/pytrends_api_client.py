
import settings
from pytrends.request import TrendReq


class PytrendsApiClient():

    pytrends = None
    cat = None
    timeframe = None
    default_kw = list()

    def __init__(self, hl='en-US', tz=360, cat=8, timeframe="all", proxy = None, user_agent = None, default_kw = []): # PROXY IS A DICTONARY {"http":nn.nn.nn.nn}
        if(proxy is None):
            self.pytrends = TrendReq( hl=hl, tz=tz )
        else:
            # self.pytrends = TrendReq( hl=hl, tz=tz, proxies = proxy, user_agent = user_agent )
            self.pytrends = TrendReq( hl=hl, tz=tz, proxies = proxy)
        self.cat = cat
        self.timeframe = timeframe
        self.default_kw = default_kw
        # print("New Api Client with Proxy: " + str(proxy) + " and User-Agent:" + str(user_agent))
        print("New Api Client with Proxy: " + str(proxy))

    def req_interest_over_time(self, kw_list):
        kw_list = self.default_kw + kw_list
        self.pytrends.build_payload(kw_list, self.cat, self.timeframe)
        res = self.pytrends.interest_over_time()
        return res
