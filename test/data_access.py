
import pymssql

class DataAccess():
    conn = None

    def __init__(self, host, user, pssw, db):
        self.conn = pymssql.connect(host, user, pssw, db)

    def __del__(self):
        self.close()

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()


    def exec_non_query(self, non_query):
        res = True
        try:
            cursor = self.conn.cursor()
            cursor.execute(non_query)
        except Exception as ex:
            print( "DataAcces Error:\r\n%s\r\n%s" % (str(ex), non_query) )
            res = False
        return res

    def exec_query(self, query):
        table = list()
        row = None
        try:
            cursor = self.conn.cursor()
            cursor.execute(query)
            try:
                row = cursor.fetchone()
            except:
                pass
            while row:
                table.append(row)
                row = cursor.fetchone()
        except Exception as ex:
            print( "DataAcces Error:\r\n%s\r\n%s" % (str(ex), query) )
        return table
