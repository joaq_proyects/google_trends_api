
import data_access

class DataLayer():

    da_obj = None

    def __init__(self, db_data):
        self.da_obj = data_access.DataAccess(db_data["host"], db_data["user"], db_data["pssw"], db_data["db"])

    def close(self):
        self.da_obj.close()

    def select_games(self):
        query = "select top(10) id,name from v_games"
        res = self.da_obj.exec_query(query)
        return res

    def insert_games_value_x_day(self, game_id, day, value):
        query = "insert into googletrends_games_trends(game_id,date,percentage) values(%s,'%s',%s)" % ( game_id, day, value, )
        res = self.da_obj.exec_non_query(query)
        self.da_obj.commit();
        return res

    def update_games_value_x_day(self, game_id, day, value):
        query = "update googletrends_games_trends set percentage=%s where game_id=%s and date='%s'" % ( value, game_id, day )
        res = self.da_obj.exec_non_query(query)
        self.da_obj.commit();
        return res

    def check_games_value_x_day(self, game_id, day):
        table = list()
        try:
            query = "select 1 from googletrends_games_trends where game_id=%s and date='%s' " % (str(game_id), day)
            table = self.da_obj.exec_query(query)
        except Exception as ex:
            # print(ex)
            pass
        return (len(table) > 0)
