
import time
import pytrends_api_client, settings, data_layer, proxies_handler
import threading, schedule


def insert_update_handler(dl_obj, game_id, data_list):

    def threaded_insert_update_handler():
        data_list_copy = list(data_list)
        for data in data_list_copy:
            if( dl_obj.check_games_value_x_day( game_id, data[0] ) ):
                method = dl_obj.update_games_value_x_day
            else:
                method = dl_obj.insert_games_value_x_day
            try:
                method( game_id, data[0], data[1] )
            except Exception as ex:
                print(ex)
    sql_thread = threading.Thread(target = threaded_insert_update_handler)
    sql_thread.start()
    return sql_thread


def loop_over_games(games_names_list, thread_num, proxies_list):
    dl_obj = data_layer.DataLayer(settings.db_data)
    proxy_in_use = 0
    api_obj = pytrends_api_client.PytrendsApiClient(proxy = proxies_list[proxy_in_use])
    data_list = list()
    aux_log = ""
    for game in games_names_list:
        kw_list = [game[1].replace("'", "''").replace("\"", "\\\"")] # game name
        retry = True
        while(retry):
            try:
                retry = False
                aux_log = "OK"
                res = api_obj.req_interest_over_time(kw_list)
            except Exception as ex:
                print("\t" + str(ex))
                aux_log = "ERR"
                if("429" in str(ex)):
                    proxy_in_use += 1
                    if(proxy_in_use < len(proxies_list)):
                        del api_obj
                        api_obj = pytrends_api_client.PytrendsApiClient(proxy = proxies_list[proxy_in_use])
                        retry = True
                        time.sleep(settings.sleep_time)
                    else:
                        print("thread:" + str(thread_num) + "->all proxies blocked!!")
                        dl_obj.close()
                        return
        if(aux_log != "ERR"):
            for index, row in res.iterrows():
                data_list.append( (index, row[0]) )
            if(aux_log == "OK" and len(data_list) == 0 ):
                aux_log = "WARN"
            print("thread:" + str(thread_num) + "->" + str(game[1].replace('\n','').replace('\r','')) + "(" + str(game[0]) + "):" + str(len(data_list)) + "::" + aux_log)
        sql_thread = insert_update_handler(dl_obj, game[0], data_list)
        data_list.clear()
        time.sleep(settings.sleep_time)
    aux_count = 0
    while(sql_thread.is_alive() and aux_count < sleep_time):
        aux_count += 1
    dl_obj.close()
    print("thread:" + str(thread_num) + "->FINISH")


def get_sublists(a_list, divisions):
    total = len(a_list)
    subq = int(total / divisions)
    new_list = list()
    ini = 0
    for div in range(0, divisions):
        new_list.append( list() )
        for i in range(ini, ini + subq):
            new_list[div].append(a_list[i])
        ini += subq
    for i in range(ini, total):
        new_list[len(new_list)-1].append(a_list[i])
    return new_list


def exec_prog():
    ph_obj = proxies_handler.ProxyHandler()
    n_threads = 6
    proxies_list = get_sublists( ph_obj.get_random_n_proxies( n_threads * 10 ), n_threads )
    dl_obj = data_layer.DataLayer(settings.db_data)
    games_names_list = dl_obj.select_games() # Returns a list of tuples
    print("list to retrieve:" + str(len(games_names_list)))
    dl_obj.close()
    games_names_list_sublisted = get_sublists(games_names_list, n_threads)
    thread_num = 1
    for sub_list in games_names_list_sublisted:
        threading.Thread(target = loop_over_games, args = (sub_list, thread_num, proxies_list[thread_num-1])).start()
        thread_num += 1


exec_prog()
schedule.every(7).days.at("15:33").do(exec_prog)
while(True):
    schedule.run_pending()
    time.sleep(3600)
