
import json,random
import requests

class ProxyHandler:

    proxies_list_source = None
    proxies_list = None


    def __init__(self):
        self.proxies_list_source = "https://www.proxy-list.download/api/v1/get?type=http"
        self.proxies_list = list()
        self.get_proxies_list()
        self.check_proxies_state()


    def check_proxies_state(self):
        if(self.proxies_list is not None):
            for p in self.proxies_list:
                p["in_use"] = False
                # IMPLEMENT SOMETHING TO CHECK HEALTH


    def get_proxies_list(self):
        res = requests.get(self.proxies_list_source)
        if(res.status_code == 200):
            print("get_proxies_list:" + str(res.status_code))
            aux_list = res.text.split("\n")
            aux_list = aux_list[0:-1]
            count = 0
            for p in aux_list:
                count += 1
                self.proxies_list.append( {"id":count,"proxy":p.replace('\r',''),"active":True,"in_use":False} )
        else:
            self.proxies_list = None


    def get_random_n_proxies( self, p_cant ):
        max_pos = len(self.proxies_list) - 1
        proxies_groups = list()
        aux_count = 0
        if(max_pos >= p_cant):
            while(len(proxies_groups) < p_cant and aux_count <= max_pos):
                aux_count += 1
                pos = random.randint(0, max_pos)
                if(self.proxies_list[pos]["active"] and not self.proxies_list[pos]["in_use"]):
                    proxies_groups.append(self.proxies_list[pos])
                    self.proxies_list[pos]["in_use"] = True
        else:
            print("arg 1 length must be lower than arg 2 (%s)" % (str(max_pos) + "<=" + str(p_cant),))
        return proxies_groups
